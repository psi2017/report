\section{Uncertainties}
\label{sec:Uncertainties}

This chapter summarizes the main sources of uncertainties that appear in our experimental setup and the subsequent analysis. Uncertainties and errors creep from different sources in the final value of the $B_{Dalitz}$, so steps were taken to limit them. From the inclusion of coincidence units to limit the background from cosmic radiation, to electronic noise that was dampened as much as possible.

\subsection{Systematic uncertainties}

\paragraph{piM1 beam line induced uncertainties}
Consulting the webpage of PSI \cite{psiwebsite}, we find all the necessary information regarding the beam our experiment will be using. 

\begin{figure}[H]
	\centering
	\includegraphics[]{figures/pim1.png}
	\caption{\label{fig:pim1}Beam composition as a function of particle momenta}
\end{figure}
Contamination of the pion beam with electrons could cause fake events if they get stopped in the target (generating a signal similar to the $\pi_0$ trigger). If these sort of events are falsely identified as pion decays, they increase the signal we see. However, the probability of an electron to emit a photon pair within the target is negligible. There is also a momentum resolution induced error, but the value is in the per mille level, so it is also highly unlucky to have a big influence in the final values.

\paragraph{Estimation of the conversion probability in the first scintillator}
In all the sections above, an event with a signal from both scintillators of a detector arm was considered to contain a charged particle. The photons were identified by placing a copper plate between the two scintillators, in order to obtain a single signal in the second scintillator due to pair production in the copper plate. This approach is based on the assumption, that photons will not be detected in the first scintillator of the arm. This assumption is not strictly true because organic scintillators have a non-zero detection efficiency for photons. We will try to roughly estimate the effect of this on our measurement in this section.

It is not useful to try an exact calculation because we do not know the exact material and therefore also not the absorption coefficients of the used scintillators. We will assume NE102A, which has an $\gamma$-absorption coefficient of $\mu=0.01 1/cm$ in our energy range (Figure \ref{fig:scintillatorAbsorption}). We will also assume, that the $\gamma$-absorption coefficient is constant for all Events. The measured thicknesses of the two relevant scintillators are $t_1=1.05~cm$ (SCB1) and $t_2=0.37~cm$ (SCS1). We would therefore expect, that a fraction of roughly
\begin{equation}
\left(1-e^{-\mu t_1}\right)+\left(1-e^{-\mu t_2}\right)=1.4~\%
\end{equation}
of all $\pi^0\rightarrow \gamma \gamma$ events will contain charge in one of the detector arms.

Since the theory value for the $\pi^0\rightarrow \gamma \gamma$ branching ratio is 98.8~\%, this should in principle increase our measured Dalitz branching ratio by 1.4~\%. As mentioned before, we do not know the scintillator material and we have assumed a constant absorption coefficient. We will therefore not use this number to correct our result.


\paragraph{Uncertainties due to mechanics}
Uncertainties that come from the mechanical setup are due to insufficient knowledge about the detectors.

\begin{itemize}
	\item Geometric alignment of the detectors, with the specifications given by the Monte Carlo simulation, was done as precisely as possible, using laser mounted cameras. Nonetheless, small angle deviations for the arms and the components with respect to either the target or the beam could result in small corrections to the final result. 
	\item An inaccurate measurement of the thicknesses of different materials could result in different rate results.
	\item A reduction in the amplitude of the readout signal is to be expected in the big calorimeter since one PMT output was damaged.
\end{itemize}

\subsection{Statistical uncertainties}
Our experiment benefited from limited beam time at PSI. Given the preparations beforehand, the commissioning and calibrations needed to run the experiment, only data from a few days could be used. Nonetheless, the statistics were enough to formulate a value for each of the target thicknesses we considered. In the chapter \ref{sec: Data analysis} each step where statistical uncertainties were prone to appear was thoroughly discussed and the sources of these errors was indicated. 
\newline
All of our data taking and detector commissioning is based on a Monte Carlo simulation that indicated the region where to best place our detectors in order to maximize rates, reduce background and compensate for limited beam time. However, we are confident that the simulation yielded the expected results and that the region we chose due to physical constraints in the laboratory space maximizes our event rates. 


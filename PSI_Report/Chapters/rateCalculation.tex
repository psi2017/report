%Section: Rate Calculation
\subsection{Calculation of the expected Performance}
The expected energy distribution and rates of both signal and background were calculated in order to obtain a theoretical estimate for a comparison to the experimental results.
\subsubsection{Energy Distribution}

For the chosen setup an energy distribution was simulated with $\alpha=-26$° and $\beta=27$°. This was done using the previously mentioned Monte Carlo simulation, and displaying only the energy of the photons which would strike both calorimeters. The resulting theoretical energy distribution can be seen Fig. \ref{fig:energydist}.

\begin{figure}[H]
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[trim= 0 0 0 0.8cm, clip, width=1\textwidth]{simulation_plots/enbcalo.pdf}
		\subcaption{Energy distribution of gamma in the big calorimeter with required coincidence}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[trim= 0 0 0 0.8cm, clip, width=1\textwidth]{simulation_plots/enscalo.pdf}
		\subcaption{Energy distribution of gamma in the small calorimeter with required coincidence}
	\end{subfigure}
	\caption{\label{fig:energydist} Energy distributions for gammas in both calorimeters were simulated with the requirement that the corresponding gamma from the \Ppizero decay will hit the other calorimeter. Here the resulting distribution for a billion events with an initial beam energy of 300 MeV is displayed.}
\end{figure}

\begin{figure}[H]
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[trim= 0 0 0 0.8cm, clip, width=1\textwidth]{simulation_plots/nocoincendist.pdf}
		\subcaption{Photonenergy distribution in the big calorimeter without required coincidence}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[trim= 0 0 0 0.8cm, clip, width=1\textwidth]{simulation_plots/encorrelation.pdf}
		\subcaption{Photonenergy distribution for both big and small calorimeter with required coincidence}
	\end{subfigure}
	\caption{\label{fig:energy}For the energy distribution of a photon in the big calorimeter without required coincidence, an uniform distribution is observed. The limits of the range from 22 MeV to 308 MeV correspond to the two extreme cases of all boosts being either in forward or in backward direction. When the energies of the two gammas are plotted with required coincidence, a linear correlation is visible, as it was expected. Two maxima are visible at energies of $E_{\Pgamma_1}=225$ MeV and $E_{\Pgamma_2}=105$ MeV and vice versa. If both calorimeters had a higher accuracy and a broader energy range, the energy correlation could have been used as an additional selection criteria for the data cuts.}
\end{figure}



There were experimental energy distributions obtained from both calorimeters, but due to the limited size of the small calorimeter, substantial energy leaking was presumably taking place in this one. The small calorimeter is too short for total containment of the particle shower at the given beam energy, what degrades the energy resolution. Therefore for a comparison of the theoretical distribution, only the experimental values of the big calorimeter were used (see Sec. \ref{ch:CalSim:sec:comparison}).

\subsubsection{Rate Calculation}
\label{ch:CalSim:sec:Calculations}

Theoretical rates were calculated so that one would have a rough estimate for both the signal and the $\Pgamma\Pgamma$-background:
\begin{align*}
R_{\text{\HepProcess{\Ppizero\to\Pgamma\Pelectron\APelectron}}}=R_{\text{\Ppizero}}\Omega Br_{Dalitz} \\
R_{\text{\HepProcess{\Ppizero \to \Pgamma \Pgamma}}}=R_{\text{\Ppizero}}\Omega Br_{\Pgamma \Pgamma} \\
\end{align*}

\noindent where $\Omega$ is the acceptance of the calorimeter setup and $R_{\text{\Ppizero}}$ the \Ppizero production rate. It is given by $r=\sigma t\rho_p n$ with the proton target density $\rho_p$, the incident \Ppiminus rate n and the cross-section $\sigma$.

\noindent This was done using
\begin{equation}
\begin{split}
\rho_p & = 4N_{A}\frac{\rho_{C_2H_4}}{M_{C_2H_4}}\\
& = 7.7289 \cdot 10^{22} cm^{-3}
\end{split}
\end{equation}
in order to calculate the density of free protons (hydrogen) in ethene, where $\rho_{C_2H_4}=0.9$ \si{\gram\per\cubic\centi\metre} and $M_{C_2H_4}=28.05$ \si{\gram\per\mol} are the density and the molar mass of ethene, respectively, and $N_A$ is the Avogadro constant.

The factor 4 arises from the fact that there are four hydrogen atoms in each ethene molecule. Furthermore the cross section $\sigma= 65\cdot 10^{-27}$ \si{\centi \metre \squared} for the  \text{\HepProcess{\Ppiminus \Pproton\to\Ppiminus\Pneutron}} conversion \cite{PDG} and $n=1.8\cdot10^7$ \si{\per\second} for the incident \Ppiminus rate \cite{psiwebsite} were used, with the current of the beam being $1.8$ mA.

Additionally the theoretical branching ratios of $Br_{Dalitz}=1.174$ \% and $Br_{\Pgamma \Pgamma}=98.823$ \% (Sec. \ref{ch:Theory:sec:Particles}) were used. For the phase space acceptance, the results from the Monte Carlo Simulation were used for the actual experimental setup as shown in Fig. \ref{fig:setup}, taking the respective geometrical acceptance of the calorimeters into account. This corresponded to $0.00622$ \% of all events being measurable, if one assumes the ideal case of every event hitting the surface of the calorimeters being registered. 

\begin{table}[H]
\caption{Expected rates for different target thicknesses}
\label{tab:ratecal}
\centering
\begin{tabular}{ l l l l }
 \hline 
& $0.5$ [cm] & $1.0$ [cm] & $1.5$ [cm]\\  \hline \hline
$R_{\text{\Ppizero (incident)}}$ & $ 45.214$ kHz & $ 90.429$ kHz & $135.64$ kHz\\ 
$R_{\text{\HepProcess{\Ppizero\to\Pgamma\Pelectron\APelectron}}}$ & $ 0.0330$ Hz & $0.0660$ Hz & $0.0990$ Hz \\ 
$R_{\text{\HepProcess{\Ppizero \to \Pgamma \Pgamma}}}$ & $ 2.7784$ Hz & $ 5.5568$ Hz  & $8.3352$ Hz\\ 
\hline
\end{tabular}
\end{table}

\subsection{Comparison to Experimental Results}
\label{ch:CalSim:sec:comparison}

Both the theoretical energy distribution and the calculated event rate can be compared to the experimental results. Although there were two calorimeters, the comparison of the energy distribution is only reasonable for the big one. The small calorimeter is too short to register the whole range of energies of the incoming particles for the incident beam energy. Also the trigger was set up to require coincidence of the two arms, so (b) of Fig. \ref{fig:energy} cannot be obtained with the experimental data, while (a) is not compared to experimental results due to the above explained leakage of the small calorimeter.\\

\subsubsection{Energy Comparison}
Both the theoretical energy distribution of the big calorimeter and the experimentally obtained distribution were fitted with two Gaussians, since this is the easiest fit while still accounting for the profile of the distribution to a certain extent.

\begin{figure}[H]
\begin{subfigure}{0.5\textwidth}
\includegraphics[trim= 0 0 0 0.8cm, clip, width=1\textwidth]{simulation_plots/theenergynew.pdf}
\subcaption{Theoretically simulated energy distribution for\\
the big calorimeter with two Gaussian fits}
\end{subfigure}
\begin{subfigure}{0.5\textwidth}
\includegraphics[trim= 0 0 0 0.8cm, clip, width=1\textwidth]{simulation_plots/expenergy.pdf}
\subcaption{Experimentally obtained energy distribution for\\
the big calorimeter with three Gaussian fits}
\end{subfigure}
\caption{\label{fig:energyfits}The theoretically obtained energy distribution has very sharp limits, with the lower being at 65 MeV, while the upper is at 260 MeV. Since there are two main peaks visible, the distribution was fitted with two Gaussians (First Gaussian: $\overline{E}=108.1 \pm 0.3$ MeV , $\sigma=19.2 \pm 0.2$ MeV, second Gaussian: $\overline{E}=218.7 \pm 0.3$ MeV, $\sigma=18.2 \pm 0.2$ MeV). The measured energy distribution for the big calorimeter was obtained after applying several cuts (required timing within $\pm 1 \sigma$ of the coincidence peak after the time walk correction, the sanity cut, and a lower limit of 25 MeV for the energy of the gamma in the small calorimeter). There are three peaks visible, and each one was fitted with a Gaussian (First main Gaussian: $\overline{E}=87.7 \pm 0.7$ MeV , $\sigma=16.6 \pm 0.6$ MeV, second main Gaussian: $\overline{E}=118.7 \pm 0.3$ MeV, $\sigma=10.7 \pm 0.1$ MeV, additional smaller Gaussian: $\overline{E}=28.6 \pm 0.3 MeV$, $\sigma=7.8 \pm 0.2$ MeV}
\end{figure}

Comparing the fits, one can easily see a reasonable accordance for the Gaussian at lower energy, whereas there is a substantial deviation in both mean energy and FWHM for the second Gaussian. Additionally there is a third, smaller peak visible at low energy in the experimental data. Possible reasons for these differences could be background, non-linearity and leakage. Since there were more events registered at low energies, there is significant background which might have not been excluded with the timing cuts. This could explain the third peak, even though its shape might suggest a different origin. The Gaussian at higher energy seems to have been shifted to lower energies while also being squeezed. Since even the big calorimeter has a very limited size in regard to the beam energy, it could be that for extremely boosted particles there is still leakage taking place. This would explain the sharp upper limit and the fairly steep gradient. Another factor to consider is the conducted energy calibration and its accuracy and potentially faulty assumptions. The energy calibration was conducted at energies substantially lower than the energy range of our signal. While its inaccuracy might explain small deviations, a potential non-linearity might have a bigger impact on the experimental results. There might be a saturation of the PMTs occuring at high energies. This might also explain the sharp upper limit, while the non-linear rise in voltage per energy would explain the squeezed shape of the second Gaussian. Furthermore one of the PMTs was broken, and therefore a considerable part of the energy was not measured altogether.\\

\subsubsection{Rate Comparison}

Before starting with the actual measurements, a beam optimization was performed in order to maximize the registered $\text{\HepProcess{\Ppizero \to \Pgamma \Pgamma}}$ event rate. 

During the optimization, it was possible to archive a rate of registered $\text{\HepProcess{\Ppizero \to \Pgamma \Pgamma}}$ events around 10~Hz, by varying the currents of the quadrupoles in the beamline. This matches our expectation from Table \ref{tab:ratecal}. 

%However, this rate is only the rate of the final Trigger and does not contain the offline event selection described in the next chapter. Therefore, it has to be reduced further.
% But since the calculated rate in Table \ref{tab:ratecal} is only based on the nominal beam current of 1.8~mA, the measured rate is reasonable when taking into account the observed rate differences of almost two orders of magnitude during the beam optimization. 
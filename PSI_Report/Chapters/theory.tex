\newpage
\section{Theoretical Aspects}
\label{ch:Theory}
This chapter will describe the theoretical foundation of the experiment, i.e. the particles involved, the \Ppizero production mechanism, the Dalitz decay and the possible scenarios for experimentally measuring the decay.
\subsection{General Remarks}
Particle physics considers matter and radiation as composed of particles.
Particles may themselves be composed of \textit{elementary particles}, which are considered indivisible. Elementary particles include quarks and leptons (fermions), as well as gauge bosons and the Higgs boson.
Hadrons are particles that are composed of either three quarks (baryon) or one quark and one anti-quark (meson).
The particles relevant in the experiment described in this work are hadrons such as pions, neutrons and protons, as well as leptons like the electron and positron, and the photon (a gauge boson).
In general, particles interact via four fundamental forces: the gravitational, the weak, the electromagnetic and the strong force.
However, the Standard Model of Particle Physics describes interactions only in terms of the latter three forces.
In particular, the interaction of light and matter as mediated by photons is described in the theory of Quantum Electro-Dynamics (QED).

Feynman diagrams are a useful way of visualizing the processes described in QED.
For example, different processes may start with the same incoming particles, but end with different particles (or even have the same outgoing particles, but have different intermediate processes): in this case, Feynman diagrams provide an easy way to write down particle reaction formulas and to calculate their various probabilities of occurring.

These theoretical predictions of different particle reactions having different probabilities are to be tested.
In the current report, the branching ratio of one specific reaction (the expected fraction of its occurrence relative to all reactions)- the Dalitz decay- is experimentally determined.

\subsection{Involved Particles}
\label{ch:Theory:sec:Particles}
\subsubsection{\Ppizero (Neutral pion)}
The neutral pion (\Ppizero) is the lightest hadron and more specifically, the lightest meson at 
\SI{134.9766(6)}{\mega\electronvolt} \cite{PDG}. It is a superposition of two up and down quarks
\begin{equation}
	|\Ppizero\rangle=\frac{1}{\sqrt[]{2}}(|\Pup \APup\rangle-|\Pdown \APdown\rangle)
\end{equation}
and has a mean lifetime of around \SI{8.4e-17}{\second} \cite{PDG}, which is shorter than the charged pion lifetime by nine orders of magnitude. This is due to the fact that charged pions decay via the weak force, whereas neutral pions decay via the electromagnetic force. The dominating decay mode for the \Ppizero with a branching ratio of $(98.823\pm0.034\%)$\cite{PDG} is the decay into two photons:
\begin{equation}
	\HepProcess{\Ppizero \to \Pgamma \Pgamma}. \label{pp}
\end{equation}
The next likeliest decay is the Dalitz decay
\begin{equation}
	\HepProcess{\Ppizero\to\Pgamma\Pelectron\APelectron}
\end{equation}

with a literature value for the branching ratio of $(1.174\pm0.035)\%$\cite{PDG}. In this case, one of the two photons undergoes an internal conversion to an \HepProcess{\Pelectron\APelectron} pair, which is why this decay is suppressed by a factor of approximately $2 \alpha$ compared to \cref{pp}. Here, $\alpha\approx\frac{1}{137}$ is the fine-structure constant. There are two photons that can undergo the conversion and the additional vertex gives a factor of $\alpha$ in the squared matrix element.

\begin{figure}
	\begin{centering}
		\begin{tikzpicture}
			\begin{feynman}
				\vertex (a1) {\(q\)};
				\vertex[right=2cm of a1] (a2);
				\vertex[right=2cm of a2] (a3);
				\vertex[below=4em of a1] (b1) {\(\overline q\)};
				\vertex[right=2cm of b1] (b2);
				\vertex[right=2cm of b2] (b3);
				\vertex[right=2cm of b3] (b4){\(e^+\)};
				\vertex[above=3cm of b4] (a4) {\(e^-\)};


				\diagram* {
					{[edges=fermion]
				(b1) -- (b2) -- (a2) -- (a1),
				(b4) -- (a3) -- (a4),
				},
				(a2) -- [boson, edge label=\(\gamma\)] (a3),
				(b2) -- [boson, edge label'=\(\gamma\)] (b3),
				};

				\draw [decoration={brace}, decorate] (b1.south west) -- (a1.north west)
				node [pos=0.5, left] {\(\pi^{0}\)};
			\end{feynman}
		\end{tikzpicture}
		\caption{Feynman diagram of the Dalitz decay }
	\end{centering}
\end{figure}

Measuring this branching ratio is the goal of the experiment described in this report. The decay mode where both photons are subject to an internal conversion is called the double Dalitz decay, with a branching ratio of \\$(3.34\pm0.16)\cdot{10^{-5}}$\cite{PDG}:
\begin{equation}
	\HepProcess{\Ppizero\to\Pelectron\APelectron\Pelectron\APelectron}.
\end{equation}

The remaining decay mode
\begin{equation}
	\HepProcess{\Ppizero\to\Pelectron\APelectron}
	\label{sup}
\end{equation}
is strongly suppressed ($BR=(6.46\pm0.33)\cdot{10^{-8}}$\cite{PDG}), due to the helicity conservation of the electromagnetic interaction: The neutral pion is a pseudo scalar particle with $J^P=0^-$. Therefore, both the total helicity and the total spin of the electron and positron have to be zero, which is impossible because their momenta are anti-parallel (see \cref{hel}).


\begin{figure}[h]
	\begin{centering}
		\begin{tikzpicture}
			\filldraw (0,0) circle (3pt) node[align=center, above]{\Ppizero};
			\draw[->](0,0)--(-4,0)node[above]{\Pelectron};
			\draw[->](0,0)--(4,0)node[above]{\Ppositron};
			\draw[->, line width=1mm] (2.5,-1) node[right]{$\lambda=-\frac{1}{2}$} -- (1.5,-1);
			\draw[->, line width=1mm] (1.5,-2) -- (2.5,-2) node[right]{$\lambda=+\frac{1}{2}$};
			\draw[->, line width=1mm] (-1.5,-2) -- (-2.5,-2);
			\draw[->, line width=1mm] (-2.5,-1) -- (-1.5,-1);
		\end{tikzpicture}
		\caption{Spin orientations in the helicity suppressed decay \cref{sup}}
		\label{hel}
	\end{centering}
\end{figure}

\subsubsection{\Pelectron \APelectron (Electron, positron)}
The electron (\Pelectron) and positron (\APelectron) are leptonic elementary particles with negative and positive elementary electric charge, respectively, and a rest mass of around \SI{511}{\kilo\electronvolt}. The Dalitz decay features an \HepProcess{\Pelectron\APelectron} pair and the experimental setup therefore needs to be able to detect them. This can be done using a scintillator and a calorimeter. 
A scintillator converts ionizing radiation into light output, which is then converted into voltage pulses that are detected.
A calorimeter makes use of the energy deposition of charged particles in matter, which is described by the Bethe-Bloch formula. If a photon or electron/positron that hits the calorimeter leads to an electromagnetic shower through pair creation and Bremsstrahlung.

\subsubsection{\Pgamma (Photon)}
The photon ($\gamma$) is a zero-rest-mass, zero-charge bosonic elementary particle and the quantum of the electromagnetic field, hereby mediating the electromagnetic force. It is part of most \Ppizero decay modes (see above) and therefore needs to be detected in the experiment. Photons can interact with matter via the photoelectric effect, Compton scattering or pair production (\HepProcess{\Pphoton \to \Pelectron \Ppositron}). In a material with atomic number Z, the cross section for the photoelectric process is proportional to $Z^5$, that for pair production proportional to $Z^2$, whereas Compton scattering goes roughly as $Z$. Therefore, the photon can be detected using a sandwiched scintillator-copper-scintillator setup: if a photon passes through this setup, it will not trigger the first scintillator (low Z) but cause the emission of charged particles via the photoelectric effect or pair production in the copper plate (high Z), which will then be detected in the second scintillator.





\subsection{Dalitz Decay}
\label{ch:Theory:sec:Dalitz}
\subsubsection{Kinematics}
\begin{figure}[h]
	\begin{centering}
		\begin{tikzpicture}
			\filldraw
			(4,2) circle (2pt) node[align=center, below] {\Ppizero};
			\draw[->]
			(6,2) node[align=right,  below] {}--(10,1.5) node[align=right,  below] {\Pelectron};

			\draw[->]
			(6,2) node[align=right,  below] {}--(10,2.5) node[align=right,  below] {\Ppositron};

			\draw[->]
			(4,2) node[align=right,  below] {}--(2,2) node[align=right,  below] {\Pphoton};
			\draw[->]
			(4,2) node[align=right,  below] {}--(5,2) node[align=right,  below] {$\Pphoton^*$};
			\draw
			(5,2) node[align=right,  below] {}--(6,2) node[align=right,  below] {};
			\draw
			(2,2) node[align=right,  below] {}--(0,2) node[align=right,  below] {};
		\end{tikzpicture}
		\caption{Dalitz decay kinematics in the \Ppizero rest frame}
	\end{centering}
\end{figure}
The Dalitz decay is the decay \HepProcess{\Ppizero\to\Pphoton\Ppositron\Pelectron}.
In the process \HepProcess{\Ppizero\to\Pphoton\Pphoton}, the momenta of the two photons are anti-parallel in the Center-of-Mass System (CMS) due to momentum conservation:
\begin{align}
	\overrightarrow{p}_{total} &= \overrightarrow{p}_{\gamma1}^* + \overrightarrow{p}_{\gamma2}^* = 0 \\
	&\Leftrightarrow \overrightarrow{p}_{\gamma1}^* = -\overrightarrow{p}_{\gamma2}^*.
\end{align}
If now one of the photons internally converts into an electron-positron pair, these two particles will move approximately parallel to the photon direction, since the electron mass is negligible ($m_e\ll\frac{m_{\pi}}{2}$). Also, the 4-momentum has to be conserved, which in pair creation is only possible with a nucleus that absorbs the recoil $p_{rec}$, which can be assumed to be small:
\begin{align}
	p_\gamma &\approx p_{\Pelectron}  + p_{\APelectron}\\
	p^2_\gamma &= 0 \Rightarrow (p_{\Pelectron} + p_{\APelectron})^2 = p^2_{\Pelectron} + p^2_{\APelectron} + 2 p_{\Pelectron} \cdot p_{\APelectron}\approx 2 p_{\Pelectron} \cdot p_{\APelectron}\\
	&\Rightarrow |\vec p_{\Pelectron}|\cdot |\vec p_{\APelectron}| - \vec p_{\Pelectron} \cdot \vec p_{\APelectron} \approx 0 \\
	&\Rightarrow \frac{\vec p_{\Pelectron}}{|\vec p_{\Pelectron}|}\cdot\frac{\vec p_{\APelectron}}{|\vec p_{\APelectron}|}  \approx 1.
\end{align}
In the laboratory frame the decay products are boosted due to the \Ppizero momentum, so that the opening angle between the photon and the electron-positron pair is $\alpha<\SI{180}{\degree}$, as shown in the following calculation.

\usetikzlibrary{quotes,angles}
\begin{figure}
	\begin{centering}
		\begin{tikzpicture}
			\draw[->](-5,0)coordinate (e)--(5,0) node[right]{z};
			\draw[dashed,->](0,0) coordinate (d)--(1,2) coordinate (h) node[right]{\Pphoton};
			\draw[dashed,->](0,0)--(-1,-2) coordinate (f) node[right]{\Pphoton};
			\draw
			pic["$\theta^*$",draw=orange,<->,angle eccentricity=1.2,angle radius=1.5cm] {angle=e--d--f}
			pic["$\pi$",draw=orange,<->,angle eccentricity=1.2,angle radius=0.5cm] {angle=h--d--f};
			\draw [->]
			(0,0) coordinate (b) node[left] {}--(1,-2) coordinate (a) node[right] {\Pphoton};
			\draw [->]
			(0,0) node[left] {} -- (2,1.5) coordinate (c) node[right] {\Pphoton}
			pic["$\alpha$",draw=orange,<->,angle eccentricity=1.2,angle radius=1cm] {angle=a--b--c};
		\end{tikzpicture}
		\caption{Opening angles in laboratory frame ($\alpha$) and CMS ($\theta^*$)}
	\end{centering}
\end{figure}

Let the z-axis be parallel to the direction of the \Ppizero and $\theta^*$ be the angle between the photon and z-direction.
Then the photon momentum in the rest frame of the \Ppizero is
\begin{equation}
	p_{\gamma}^*=\frac{1}{2}m_{\Ppizero}\begin{pmatrix}1 \\ \pm\sin(\theta^*) \\ 0 \\\pm\cos(\theta^*)\end{pmatrix}
\end{equation}
and
\begin{equation}
	p_{\gamma}=\Lambda p_{\gamma}^*\label{boost}
\end{equation}
in the laboratory system, where $\Lambda$ is the boost matrix
\begin{equation}
	\Lambda=\begin{pmatrix}
		\gamma & 0 & 0 & \gamma\beta\\
		0 & 1 & 0 & 0\\
		0 & 0 & 1 & 0\\
		\gamma\beta & 0 & 0 & \gamma\\
	\end{pmatrix}.
\end{equation}
From the momentum given in \cref{boost}, one can calculate the
photon energy
\begin{align}
	E_{\gamma}=\gamma \frac{1}{2} m_{\Ppizero}(1+\beta\cos(\theta^*))
	\label{edistri}
\end{align}
\begin{align}
	\Rightarrow E_{\gamma}^{\text{min/max}}=\gamma \frac{1}{2} m_{\Ppizero}(1\pm\beta)
\end{align}
and the opening angle in the laboratory frame
\begin{align}
	\alpha=\arccos(\frac{\vec p_{\gamma1}\cdot\vec p_{\gamma2}}{|\vec p_{\gamma1}| |\vec p_{\gamma2}|}).
	\label{labangle}
\end{align}
Where the two photon momenta in the laboratory are
\begin{align}
	\vec{p}_{\gamma1/2}=\frac{1}{2}m_{\Ppizero}\begin{pmatrix}
	\pm\sin(\theta^*) \\ 0 \\\gamma\beta\pm\cos(\theta^*)\end{pmatrix}.
\end{align}
The larger the opening angle $\alpha$, i.e. the smaller $\theta^*$, the smaller the corresponding phase space: $\theta^*=\frac{\pi}{2}$ describes a full circle, while $\theta^*=0$ corresponds to only one point. Therefore, the  most likely opening angle is the minimal opening angle $\alpha_{min}$ at $\theta^*=\frac{\pi}{2}$.

\begin{figure}
	\begin{centering}
		\begin{tikzpicture}
			\draw[->](0,0)--(8,0) node[right]{E};
			\draw[->](0,0)--(0,3) node[left]{rate};
			\draw (2,0) node[below]{$E_{min}\approx\SI{55}{\mega\electronvolt}$}--(2,2)--(6,2)--(6,0) node[below]{$E_{max}\approx\SI{83}{\mega\electronvolt}$};

		\end{tikzpicture}
		\caption{Energy distribution as described in \cref{edistri}}
	\end{centering}
\end{figure}

\subsection{Pion Production}
The experiment uses a beam of charged pions at piM1-beamline\cref{psi website} at the Paul-Scherrer Institute (PSI), Switzerland.
Neutral pions can be produced via the charge exchange reactions (CEX)
\begin{equation}
	\HepProcess{\Ppiplus \Pneutron \to \Ppizero \Pproton}
\end{equation}
and
\begin{equation}
	\HepProcess{\Ppiminus \Pproton \to \Ppizero \Pneutron}.\label{scA}
\end{equation}
The produced neutral pions will decay immediately after production due to their short lifetime. Consider a hydrogen target which enables the second reaction (\cref{scA}) in order to calculate the energy of the produced \Ppizero. In the experiment a plastic target (CH$_2$) is used because pure hydrogen would be too dangerous.

The rest masses of the \Pproton and \Ppiminus are $m_{\Pproton}=\SI{938.3}{\mega\electronvolt}$ and $m_{\Ppiminus}=\SI{139.6}{\mega\electronvolt}$, totalling $\SI{1077.9}{\mega\electronvolt}$.
The rest masses of the \Pneutron and \Ppizero are $m_{\Pneutron}=\SI{939.6}{\mega\electronvolt}$ and $m_{\Ppizero}=\SI{135}{\mega\electronvolt}$, totalling $\SI{1074.6}{\mega\electronvolt}$.
The difference is $Q=\SI{3.3}{\mega\electronvolt}$.
The CMS energy denoted $\sqrt{s}$ of a two-particle system is
\begin{equation}
	\sqrt{s}=m_1\sqrt{1+\frac{p^2}{m_1^2}} + m_2\sqrt{1+\frac{p^2}{m_2^2}} \label{s}
\end{equation}
where $m_1$ and $m_2$ are the rest masses of the two particles. With $p \ll m$ this gives
\begin{equation}
	\sqrt{s}\approx m_1\left(1+\frac{p^2}{2m_1^2}\right) + m_2\left(1+\frac{p^2}{2m_2^2}\right)
\end{equation}
and thus
\begin{equation}
	p^2=2(\sqrt{s} - m_1 - m_2)\bar m\approx 2 Q \bar m
\end{equation}
where
\begin{equation}
	\bar{m} = \frac{m_1m_2}{m_1+m_2}.
\end{equation}

Inserting the \Ppizero and \Pneutron mass, this yields a CMS-momentum for the \Ppizero of $p_A=\SI{27.9}{\mega\electronvolt}$.
The decay products of the pion are boosted; the value $p_A$ will be used in explaining scenario A (\cref{scA}) of the two considered experiment scenarios in the next section.


Neutral pion production may also occur via the \PDelta resonance (as in scenario B (\cref{scB}) of the next section):
\begin{equation}
	\HepProcess{\Ppiminus \Pproton\to\HepParticleResonance{\PDelta}{1232}{}{}\to\Ppizero\Pneutron}.  \label{scB}
\end{equation}
With $p_{\text{beam}}=\sqrt[]{E_{\text{beam}}^2-m_{\Ppiminus}^2}$ and $\sqrt[]{s}=\SI{1232}{\mega\electronvolt}$ one finds that the resonant energy
\begin{equation}
	s=\left(\begin{pmatrix}E_{\text{beam}}\\\vec{p}_{\text{beam}}\end{pmatrix}+\begin{pmatrix}m_P\\\vec{0}\end{pmatrix}\right)^2=m_P^2+m_{\Ppiminus}^2+2m_PE_{\text{beam}}
\end{equation}
is equivalent to a \Ppiminus beam energy of $E_{\text{beam}}\approx\SI{300}{\mega\electronvolt}$.
Rearranging \cref{s} gives
\begin{equation}
	p=\sqrt[]{\frac{s^2-m_1^2-m_2^2-4m_1^2m_2^2}{4s}}.
\end{equation}
Therefore, the CMS-momentum of the \Ppizero when using the \PDelta resonance to produce \Ppizero particles is $p_B^*\approx\SI{207}{\mega\electronvolt}$.
Because of the second boost from the process \HepProcess{\Ppiminus \Pproton\to\HepParticleResonance{\PDelta}{1232}{}{}}, this corresponds to $\SI{157}{\mega\electronvolt}\leq p_B\leq\SI{266}{\mega\electronvolt}$ in the laboratory system, depending on the angle of the \Ppizero to the \Ppiminus beam.
This is the value relevant for scenario B (\cref{scB}).


\subsubsection{\Ppizero production scenarios}
\label{ch:Theory:sec:Scenarios}
There are two ways by which the \Ppizero can be produced at $\pi M1$.
In scenario A (\cref{scA}) the \Ppiminus is stopped in the target on a time scale of approximately \SI{1}{\nano\second} before the reaction \HepProcess{\Ppiminus \Pproton \to \Ppizero \Pneutron} takes place. In scenario B (see \cref{scB}), where the $\Delta$-resonance is exploited, the \Ppiminus has sufficient kinetic energy to hit the nuclear resonance $J^P=\frac{3}{2}^+$ at CMS-energy $s=\SI{1232}{\mega\electronvolt}$, where the reaction cross section has a sharp maximum.

In order to find the Lorentz-factor $\gamma=\frac{E}{m}$ and velocity $\beta=\frac{p}{E}$ for both scenarios, the \Ppizero momenta $p_{A/B}$ calculated above are inserted. \cref{labangle} gives the minimal opening angles using 
\begin{equation}
	\bar\alpha^{\text{min}}_A=\SI{156}{\degree}
\end{equation}
\begin{equation}
	\bar\alpha^{\text{min}}_B=\SI{66}{\degree},
\end{equation}
where $\alpha^{\text{min}}$ is averaged over all angles of the \Ppizero momentum direction with respect to the beam in scenario B.
This minimal opening angle is a first estimation of the most likely opening angle. The probability distribution of $\alpha$ is determined via Monte Carlo simulation in the next chapter.\\
Since the minimal opening angle is much smaller in scenario B than in scenario A, the detectors are positioned closer to the beam which increases the background. 
However, in scenario A the CH$_2$ target has to be thick enough to stop the \Ppiminus. This leads to more background from external conversions to \Ppositron\Pelectron of one of the photons in \HepProcess{\Ppizero\to\Pphoton\Pphoton}
: The Dalitz decay rate is proportional to the target thickness t while the background from photon conversions in the target material is proportional to the thickness squared.
\begin{align*}
	R_{\text{Dalitz}}=r\Omega B \\
	R_{\text{conversion}}=r\Omega kt
\end{align*}
Here, $\Omega$ is the solid angle, k the conversion probability in the target per thickness and r the \Ppizero production rate, which is given by $r=\sigma t\rho n$ with  the target density $\rho$, the rate n of negative pions hitting the target and the cross-section $\sigma$ of the process in \cref{scA}.

To optimize the Dalitz rate over conversion, a thin target is preferable, which is one reason why scenario B was chosen for the experiment. Also, the expected rate of events is higher in scenario B. The third reason is that scenario A has already been tried by a different group and we wanted to explore the second scenario.


\section{Waveform analysis}

\noindent Using data acquisition system data was stored into binary files during the experiment at PSI. The task that follows is to get from binary files to signal characteristics used in data analysis to determine the Dalitz decay branching ratio. For that purpose two waveform analysers were implemented: simple and improved waveform analyser. You can see their main differences in the lower table:

\begin{center}
\begin{table}[bh]
\centering
\begin{tabular}{ c|c } 
 \hline
 Simple waveform analysis & Improved waveform analysis \\
  \hline
  \rule{0pt}{3ex}  
    no baseline analysis & baseline analysis \\
  \rule{0pt}{3ex}  
 hard coded threshold peak finder &  baseline based peak finder\\ 
 \rule{0pt}{3ex}  
 finding only first peak & multiple peak finder \\ 
 \rule{0pt}{3ex}  
  threshold rise/fall edge finder & interpolated rise/fall edge finder \\ 
 \rule{0pt}{3ex}  
 no peak timing synchronization & peak timing synchronization \\ 
 \rule{0pt}{3ex} 
 one program & modular code \\ 
\end{tabular}
\caption{Simple and improved waveform analyser comparison.}
\end{table}
\end{center}

The overview of software modules in analysers that are used on each data run can be found below:

\begin{figure}[H]
\begin{center}
\includegraphics[width=0.85\linewidth]{software_plots/psi_software_modules.pdf}
\caption{Diagram of software modules developed and used for data processing.}
\end{center}
\end{figure}


\noindent The improved waveform analysis was implemented and tested though decision was made not to use it in data analysis since the results of the Dalitz decay branching ratio obtained by using the data provided by simple waveform analysis are satisfactory and extra effort needed to redo analysis was to high with respect to the corrections that would be obtained. Here in this chapter, explanation can be found how the improved waveform analyser was implemented and how it improves waveform signal analysis. Most of the methods used in the simple waveform analyser are the same as in improved version but the improved analyser is a modular program and can be explained much easier than the simple analyser. However the algorithms and waveform analysis concepts are the same in both versions of waveform analyser.\\

\noindent Waveform analysis process starts by loading binary files to the memory. The binary files contain encoded waveform signals with encoding format explained in chapter 3 (Converting and writing of the data Table 2). As the first step of the waveform analysis process, C++ Data converter module is used to create a ROOT file that contains all the raw waveform signals for all the detector channels that were used in our experiment. Raw waveforms that are obtained from our scintillators and calorimeters (channel 1 - 12) have the following shape:

\begin{figure}[H]
\begin{center}
\includegraphics[width=1.0\linewidth]{software_plots/example_waveform.pdf}
\caption{Comparison of waveforms from different channels.}
\end{center}

\begin{center}
\includegraphics[width=1.0\linewidth]{software_plots/raw_waveforms.pdf}
\caption{Comparison of different waveforms for specific channel.}
\end{center}
\end{figure}

\noindent The waveforms are similar for different channels and different events. They slightly differ in the amount of noise and the intensity of peaks. For the veto scintillator and beam condition monitor scintillator (BCM) the waveform peaks are much thinner then for the rest of channels and have the following shape:


\begin{figure}[H]
\begin{center}
\includegraphics[width=\linewidth]{software_plots/veto_BCM.pdf}
\caption{Waveform example from BCM and veto scintillator.}
\end{center}
\end{figure}


\subsection{Baseline analysis}

\noindent   Peak finding in waveforms has to be automatised, and for that, method had to be developed that can decide when signal contained a peak. For that, the baseline for each run and each channel has to be defined first. By plotting all waveforms for each channel from a single run into a 2D ROOT histogram, it is seen that most of the signal is centred around value $8200$. By zooming into the region around $8200$ the baseline is clearly present in waveform signals:

\begin{figure}[H]
\begin{center}
\includegraphics[width=\linewidth]{software_plots/all_waveforms_histogram.pdf}
\caption{2D histogram of all waveforms for specific run with clear sign of baseline around $8200$.}
\end{center}
\end{figure}
\begin{figure}
\begin{center}
\includegraphics[width=\linewidth]{software_plots/all_waveforms_histogram_zoom.pdf}
\caption{Enlarged region of 2D histogram of all waveforms around baseline.}
\end{center}
\end{figure}

\noindent The above plot is a proof why multiple peak analysis is needed since most events contain multiple peaks. To find the value of the above baseline for each channel C++ Module Baseline analysis was used. The module:
\begin{enumerate}
\item{loads the waveforms from Waveforms.root file to the memory,}
\item{calculates the average of half of the pre-samples (in our case first 25 points of the waveform) and stores obtained value as the waveform baseline,}
\item{calculates the average of the voltage signal in the waveform (sum over all voltage points $/$ num of points) and stores the obtained value as the waveform average,}
\item{stores the calculated waveform baselines and waveform averages in a ROOT file.} 
\end{enumerate}

\noindent Once the whole run is analysed the obtained distributions give the mean of baseline that is then used as the average of run baseline (for determination of the rise edge) and the mean of waveform average distribution as variable that helps identify and remove flat waveforms. As an example for run 753 the following baseline distributions were obtained:

\begin{figure}[H]
\begin{subfigure}{0.5\linewidth}
  \centering
  \includegraphics[width=\linewidth]{software_plots/BCM_baseline.pdf}
\end{subfigure}%
\begin{subfigure}{0.5\linewidth}
  \centering
  \includegraphics[width=\linewidth]{software_plots/BCM_average.pdf}
\end{subfigure}%

\begin{subfigure}{0.5\linewidth}
  \centering
  \includegraphics[width=\linewidth]{software_plots/detector_baseline.pdf}
\end{subfigure}%
\begin{subfigure}{0.5\linewidth}
  \centering
  \includegraphics[width=\linewidth]{software_plots/detector_average.pdf}
\end{subfigure}%

\begin{subfigure}{0.5\linewidth}
  \centering
  \includegraphics[width=\linewidth]{software_plots/veto_baseline.pdf}
\end{subfigure}%
\begin{subfigure}{0.5\linewidth}
  \centering
  \includegraphics[width=\linewidth]{software_plots/veto_average.pdf}
\end{subfigure}%
\caption{Left: baseline distributions for channel 0, 3 and 13. Right: waveform average distributions for channel 0, 3 and 13.}
\end{figure}

\noindent For the rest of channels the distributions have the same shape as the one for channel 3 due to similar type of signal we are measuring. The start (rise edge) and end (fall edge) of the peak are then defined as the position where our signal crosses the specific threshold. The threshold is defined as $3\sigma$ above the baseline of our signal.

\subsection{Rise and fall edge finder}

\noindent In the experiment waveform signals were sampled with digitiser every $10$ $ns$. So when rise/fall edge is determined using the threshold method the exact rise/fall edge positions is missed by few $ns$. By finding the exact point in time when the rise/fall edge has occurred one can reduce uncertainties on rise/fall edges and thus increase the accuracy of later analysis. \\

\noindent To increase the accuracy the rise/fall edge was interpolated to the exact place where the waveform crossed the threshold of $3\sigma$ above baseline. In the case of the rise edge finder: the algorithm will always overshoot the correct rise edge and find $t_n$ as rise edge but the correct rise edge is at $t_r$ as seen below:
    
\begin{figure}[H]
\includegraphics[width=\linewidth]{software_plots/rise_edge_interpolation.pdf}
\caption{Example of rise-edge determination.}
\end{figure}     
     
\noindent Fall edge finder: will also always overshoot the correct fall edge as seen below:

\begin{figure}[H]
\includegraphics[width=\linewidth]{software_plots/fall_edge_interpolation.pdf}
\caption{Example of fall-edge determination.}
\end{figure}
 
     
\noindent Same interpolation approach for the rise and fall edge can be used. Slopes involved in the interpolation will just have a different sign. In the interpolation scheme the time positions $t_n = 10 n$, $t_{n-1} = 10 (n - 1)$ and voltage points: $V_n = points.at(n)$, $V_{n - 1} = points.at(n -1)$ were taken. The slope $k$ between the voltage point before the edge identified by threshold and point after identified edge was then calculated. Using the slope $k$ the offset $m$ was also determined:
     
     $$ k = \frac{\Delta V}{\Delta t} = \frac{V_n - V_{n-1}}{t_n - t_{n-1}} \quad \quad m = V_n - kt_n$$
     
\noindent The correct rise/fall edge is at point $r$  where baseline $V_r = baseline + 3\sigma$ is exceeded or at time $t_r$ (interpolated rise/fall edge):
     
     $$ V_r = k t_r + m \quad\quad \rightarrow \quad\quad t_r = \frac{V_r - m}{k} $$


\subsection{Peak finder}

For BCM and Veto channel peaks were identified at positions where the threshold was exceeded since all the peaks are sharp deltas. For the rest of the scintillator and calorimeter channels, peak finder searches for the maximum value between each rise and fall edge in the waveform that were obtained from interpolation rise/fall edge finder.

\subsection{Integration}

Next physical quantity that is needed for later analysis is the integral between the interpolated rise and fall edge. Since the interpolation of the integration boundaries is done the careful summation of intervals has to be performed:

\begin{figure}[H]
\begin{center}
\includegraphics[width=\linewidth]{software_plots/square_integration.pdf}
\caption{Overview of waveform integration regions.}
\end{center}
\end{figure}

\noindent Integration is split into three intervals as seen above and obtain trough the following integral:

$$\int_{x_{min_i}}^{x_{max_i}}  = \int_{x_{min}}^{x_{max}} + \int_{x_{min_i}}^{x_{min}} - \int_{x_{max_i}}^{x_{max}}$$

$$ \int_{x_{min}}^{x_{max}} = \left(\sum_{x_i = x_{min}}^{i = x_{max}} signal(x_i)\right) dt$$
\noindent First bin:
$$ \int_{x_{min_i}}^{x_{min}}  = (x_{min} - x_{min_i}) \frac{signal(x_{min}) - signal(x_{min_i})}{2}$$

\noindent Last bin:
$$ \int_{x_{max_i}}^{x_{max}}  = (x_{max} - x_{max_i}) \frac{signal(x_{max_i}) - signal(x_{max})}{2}$$

\subsection{Storage}

\noindent Once all the waveforms from all channels are analysed multiple peak information is stored to ROOT file. The following characteristics that are needed for later data analysis are stored:
\begin{itemize}
\item{channel 7, 8 $\rightarrow$ integral}
\item{channel 0 - 13 $\rightarrow$ peak position}
\item{channel 1 - 13 $\rightarrow$ interpolated rise edge position}
\end{itemize}

\subsection{Usage of multiple peaks data}

\noindent Secondary peaks in the waveforms represent background in our signal. So by analysis of the spectrum of secondary peaks one can obtain the background energy spectrum. By subtracting the background energy spectrum from the energy spectrum obtained from first waveform peaks the clean signal is obtained. Multiple peak information can also be used to perform timing analysis of the beam counter (BCM and veto detector combined). That information would yield extra improvements in final analysis of Dalitz decay branching ratio.

